<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use App\Note;

class NoteTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex()
    {
        $notes = factory(Note::class, 6)->create();
        
        $response = $this->get('/notes');

        $response->assertStatus(200);
        $response->assertViewHas('notes', function ($notes) {
            $this->assertCount(6, $notes->toArray());
            return true;
        });
    }

    public function testShow()
    {
        $note = factory(Note::class)->create();
        
        $response = $this->get("/notes/$note->id");

        $response->assertStatus(200);
        $response->assertViewHas('note', function ($viewNote) use ($note) {
            $this->assertEquals($note->content, $viewNote->content);
            return true;
        });
    }

    public function testCreate()
    {
        $response = $this->get("/notes/create");

        $response->assertStatus(200);
    }

    public function testEdit()
    {
        $note = factory(Note::class)->create();
        
        $response = $this->get("/notes/$note->id/edit");

        $response->assertStatus(200);
        $response->assertViewHas('note', function ($viewNote) use ($note) {
            $this->assertEquals($note->content, $viewNote->content);
            return true;
        });
    }

    public function testStore()
    {
        $noteData = factory(Note::class)->make()->toArray();
        
        $response = $this->post("/notes", $noteData);

        $response->assertStatus(302);
        $response->assertRedirect(route('notes.index'));
        $this->assertDatabaseHas('notes', $noteData);
    }

    public function testStoreWithEmptyContent()
    {
        $response = $this->post("/notes");

        $response->assertStatus(302);
        $response->assertSessionHasErrors([ 'content' ]);
    }

    public function testUpdate()
    {
        $note = factory(Note::class)->create([
            'content' => 'before',
        ]);
        
        $response = $this->put("/notes/$note->id", [
            'content' => 'after',
        ]);

        $response->assertStatus(302);
        $response->assertRedirect(route('notes.index'));
        $this->assertDatabaseHas('notes', [
            'id' => $note->id,
            'content' => 'after',
        ]);
    }

    public function testUpdateWithEmptyContent()
    {
        $note = factory(Note::class)->create([
            'content' => 'before',
        ]);
        
        $response = $this->put("/notes/$note->id");

        $response->assertStatus(302);
        $response->assertSessionHasErrors([ 'content' ]);
    }

    public function testDelete()
    {
        $note = factory(Note::class)->create();
        
        $response = $this->delete("/notes/$note->id");

        $response->assertStatus(302);
        $this->assertDatabaseMissing('notes', [
            'id' => $note->id,
        ]);
    }
}
