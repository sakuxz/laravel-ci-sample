<?php

namespace Tests\Feature\Api;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use App\Note;

class NoteTest extends TestCase
{
    use RefreshDatabase;

    public function testIndex()
    {
        $notes = factory(Note::class, 6)->create();
        
        $response = $this->get('/api/notes');

        $response->assertStatus(200);
        $response->assertJsonStructure([
            'notes' => [
                '*' => [
                    'content'
                ]
            ]
        ]);
        $this->assertCount(6, $response->decodeResponseJson()['notes']);
    }

    public function testStore()
    {
        $noteData = factory(Note::class)->make()->toArray();

        $response = $this->json('POST', '/api/notes', $noteData);

        $response->assertStatus(200);
        $response->assertJson([
            'note' => [
               'content' => $noteData['content']
            ]
        ]);
        $this->assertDatabaseHas('notes', $noteData);
    }

    public function testStoreWithEmptyContent()
    {
        $response = $this->json('POST', "/api/notes");

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
               'content' => [],
            ]
        ]);
    }

    public function testUpdate()
    {
        $note = factory(Note::class)->create([
            'content' => 'before',
        ]);

        $response = $this->json('PUT', "/api/notes/$note->id", [
            'content' => 'after',
        ]);

        $response->assertStatus(200);
        $response->assertJson([
            'note' => [
                'content' => 'after',
            ]
        ]);
        $this->assertDatabaseHas('notes', [
            'id' => $note->id,
            'content' => 'after',
        ]);
    }

    public function testUpdateWithEmptyContent()
    {
        $note = factory(Note::class)->create([
            'content' => 'before',
        ]);
        
        $response = $this->json('PUT', "/api/notes/$note->id");

        $response->assertStatus(422);
        $response->assertJson([
            'errors' => [
               'content' => [],
            ]
        ]);
    }
    
    public function testShow()
    {
        $note = factory(Note::class)->create();

        $response = $this->get("/api/notes/$note->id");

        $response->assertStatus(200);
        $response->assertJson([
            'note' => [
                'content' => $note->content,
            ]
        ]);
    }

    public function testDelete()
    {
        $note = factory(Note::class)->create();

        $response = $this->delete("/api/notes/" . $note->id);

        $response->assertStatus(200);
        $response->assertJson([
            'note' => [
                'content' => $note->content,
            ]
        ]);
        $this->assertDatabaseMissing('notes', [
            'id' => $note->id,
            'content' => $note->content,
        ]);
    }
}
