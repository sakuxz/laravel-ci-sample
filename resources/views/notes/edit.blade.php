<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Note</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    </head>
    <body>
        <div style="max-width: 500px; margin: auto;">
            <h1 class="mb-3 mt-4">Update Note</h1>
    
            @include('notes._form', [
                'note' => $note,
                'action' => route('notes.update', ['id' => $note->id]),
                'method' => 'put',
            ])
        </div>
    </body>
</html>
