<form method="POST" action="{{ $action }}">
    <div class="form-group">
        <label for="content">Content</label>
        <textarea
            class="form-control {{ $errors->has('content') ? 'is-invalid' : '' }}"
            id="content"
            rows="8"
            name="content">{{ $note->content }}</textarea>
        @if($errors->has('content'))
            <span class="invalid-feedback">
                <strong>{{ $errors->first('content') }}</strong>
            </span>
        @endif
    </div>
    @csrf
    @method($method)
    <button type="submit" class="btn btn-primary">Submit</button>
    <a href="{{ route('notes.index') }}" class="btn btn-light">Back</a>
</form>
