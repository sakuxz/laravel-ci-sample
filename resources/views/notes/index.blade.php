<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Notes</title>

        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.0/css/bootstrap.min.css" integrity="sha384-9gVQ4dYFwwWSjIDZnLEWnxCjeSWFphJiwGPXr1jddIhOegiu1FwO5qRGvFXOdJZ4" crossorigin="anonymous">
    </head>
    <body>
        <div style="max-width: 500px; margin: auto;">
            <h1 class="mb-3 mt-4">Notes</h1>
    
            <ul class="list-group">
                @foreach($notes as $note)
                    <li class="list-group-item d-flex justify-content-between align-items-center">
                        <a href="{{ route('notes.show', ['id' => $note->id]) }}">
                            {{ substr($note->content, 0, 30) }}...
                        </a>
                        <div>
                            <a href="{{ route('notes.edit', ['id' => $note->id]) }}" class="btn btn-outline-primary btn-sm">Edit</a>
                            <form 
                                style="display: inline;"
                                method="POST"
                                action="{{ route('notes.destroy', ['id' => $note->id]) }}">
                                @csrf
                                @method('delete')
                                <button onclick="return confirm('確定要刪除？');" type="submit" class="btn btn-outline-danger btn-sm">Delete</button>
                            </form>
                        </div>
                    </li>
                @endforeach
                <a href="{{ route('notes.create') }}" class="btn btn-primary mt-3">Create</a>
            </ul>
        </div>
    </body>
</html>
