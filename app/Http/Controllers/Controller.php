<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected function renderJsonOrBlade($payload, $blade = null)
    {
        $request = request();

        if ($blade === null || $request->wantsJson() || $request->is('api/*')) {
            return response()->json($payload);
        }

        return view($blade, $payload);
    }

    protected function renderJsonOrRedirectTo($payload, $url = null)
    {
        $request = request();

        if ($url === null || $request->wantsJson() || $request->is('api/*')) {
            return response()->json($payload);
        }

        return redirect($url);
    }

    protected function renderJsonOrRedirectBack($payload)
    {
        $request = request();

        if ($request->wantsJson() || $request->is('api/*')) {
            return response()->json($payload);
        }

        return redirect()->back();
    }
}
