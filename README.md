# Laravel CI Sample

## Docker for Development

### Initialize

```sh
docker-compose -f docker-compose.local.yml up -d
docker-compose -f docker-compose.local.yml exec workspace bash

# in workspace
> composer install
> cp docker/.env.example .env
> php artisan key:generate
> php artisan serve --host 0.0.0.0
```

### Laravel app

http://localhost:8000

### phpMyAdmin

http://localhost:8888